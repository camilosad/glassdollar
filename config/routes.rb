Rails.application.routes.draw do

  resources :reviews
  root to: 'visitors#index'

  devise_for :users
  resources :companies

  get  'founders/:id', to: 'founders#show', as: 'founder'
  get  'investors', to: 'investors#index'
  get  'investors/:id', to: 'investors#show', as: 'investor'

end
