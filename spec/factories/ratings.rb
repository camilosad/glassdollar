FactoryGirl.define do
  factory :rating do
    founder "MyString"
    investor "MyString"
    rate "MyString"
    category nil
  end
end
