FactoryGirl.define do
  factory :company do
    name "MyString"
    investment_size "MyString"
    industry nil
    location "MyString"
    stage "MyString"
  end
end
