puts 'Cleaning database..'

Rating.delete_all
Review.delete_all
Category.delete_all
Company.delete_all
Industry.delete_all
User.delete_all

puts 'Seeding database...'

# investors
User.create email: 'investor1@email.com', name: 'John Snow', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor2@email.com', name: 'Eddard Stark', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor3@email.com', name: 'Hodor', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor4@email.com', name: 'Tyrion Lannister', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor5@email.com', name: 'Joffrey Baratheon', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor6@email.com', name: 'Khal Drogo', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor7@email.com', name: 'Melisandre', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor8@email.com', name: 'Ygritte', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor9@email.com', name: 'Petyr Baelish', role: 'investor', password: '123456', location: Faker::Address.country
User.create email: 'investor10@email.com', name: 'Theon Greyjoy', role: 'investor', password: '123456', location: Faker::Address.country


# founders
User.create email: 'founder1@email.com', name: 'Arya Stark', role: 'founder', password: '123456', location: Faker::Address.country
User.create email: 'founder2@email.com', name: 'Robb Stark', role: 'founder', password: '123456', location: Faker::Address.country
User.create email: 'founder3@email.com', name: 'Bronn', role: 'founder', password: '123456', location: Faker::Address.country
User.create email: 'founder4@email.com', name: 'Margaery Tyrel', role: 'founder', password: '123456', location: Faker::Address.country
User.create email: 'founder5@email.com', name: 'Daenerys Targaryen', role: 'founder', password: '123456', location: Faker::Address.country

# industries
5.times do |index|
  Industry.create name: Faker::Commerce.unique.department
end

# constants to avoid extra querying
FOUNDERS = User.founders
INVESTORS = User.investors
INDUSTRIES = Industry.all

# companies
5.times do |index|
  Company.create name: Faker::Company.unique.name,
    investment_size: Faker::Number.decimal(5, 2),
    industry: INDUSTRIES.sample,
    location: Faker::Address.country,
    founder: FOUNDERS[index]
end

# categories
Category.create name: "Vision"
Category.create name: "Availability"
Category.create name: "Networking"
Category.create name: "Communication"
Category.create name: "Reliability"

# reviews
10.times do |index|
  Review.create founder: FOUNDERS.sample, investor: INVESTORS.sample, comment: Faker::Lorem.sentence
end

REVIEWS = Review.all
RATES = [1, 2, 3, 4, 5]
CATEGORIES = Category.all

# ratings
15.times do |index|
  Rating.create rate: RATES.sample, category: CATEGORIES.sample, review: REVIEWS.sample
end