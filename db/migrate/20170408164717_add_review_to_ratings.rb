class AddReviewToRatings < ActiveRecord::Migration[5.0]
  def change
    add_reference :ratings, :review, foreign_key: true
  end
end
