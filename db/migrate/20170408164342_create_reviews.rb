class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.belongs_to :founder
      t.belongs_to :investor
      t.text :comment

      t.timestamps
    end
  end
end
