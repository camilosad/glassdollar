class RemoveUserReferencesFormRatings < ActiveRecord::Migration[5.0]
  def change
    remove_column :ratings, :founder_id, :integer
    remove_column :ratings, :investor_id, :integer
  end
end
