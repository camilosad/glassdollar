class CreateRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :ratings do |t|
      t.belongs_to :founder
      t.belongs_to :investor
      t.integer :rate
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
