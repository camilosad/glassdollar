class CreateInvestments < ActiveRecord::Migration[5.0]
  def change
    create_table :investments do |t|

      t.belongs_to :investor
      t.references :company, foreign_key: true
      t.integer :value
      t.datetime :date

      t.timestamps
    end
  end
end
