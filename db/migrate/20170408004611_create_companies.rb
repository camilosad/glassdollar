class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.decimal :investment_size
      t.references :industry, foreign_key: true
      t.string :location
      t.string :stage
      t.belongs_to :founder

      t.timestamps
    end
  end
end
