json.extract! company, :id, :name, :investment_size, :industry_id, :location, :stage, :created_at, :updated_at
json.url company_url(company, format: :json)
