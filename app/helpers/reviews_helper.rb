module ReviewsHelper

  def rate_range
    [[1, 1], [2, 2], [3, 3], [4, 4] , [5, 5]]
  end

  def reviews_text(count)
    return 'No reviews' if count == 0
    return '1 review' if count == 1
    return "#{count} reviews"
  end
end
