module UsersHelper
  def role_options
    [['Founder', 'founder'], ['Investor', 'investor']]
  end

  def can_edit_company(company)
    current_user.founder? && company.founder.id == current_user.id
  end

  def company_owner?
    current_user.founder? && current_user.companies.any?
  end
end
