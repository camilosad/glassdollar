class InvestorsController < ApplicationController
  before_action :set_investor, only: [:show]

  def index
    @query = User.investors.ransack(params[:q])
    @investors = @query.result(distinct: true)
    @investors = @investors.sort_by { |i| i.overall_rating }.reverse
  end

  def show
    @review = @investor.received_reviews.build
    @review.founder = current_user
    @categories = Category.pluck(:name, :id)
  end

  private

  def set_investor
    @investor = User.find(params[:id])
  end
end
