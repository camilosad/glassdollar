class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def after_sign_up_path_for(resource)
    resource.founder? ? new_company_path : companies_path
  end

  def after_sign_in_path_for(resource)
    if resource.founder?
      return investors_path if resource.companies.any?
      return new_company_path
    else
      return companies_path
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :role])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :avatar, :location])
  end

end
