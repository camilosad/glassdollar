class ReviewsController < ApplicationController

  def create
    @review = Review.new(review_params)

    respond_to do |format|
      @review.save
      puts @review.errors.full_messages
      format.html { redirect_to investor_path(@review.investor) }
    end
  end

  private

  def review_params
    params.require(:review).permit(
      :comment, :founder_id, :investor_id,
      ratings_attributes: [:rate, :category_id, :review, :_destroy]
    )
  end
end
