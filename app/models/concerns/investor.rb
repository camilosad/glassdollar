module Investor
  extend ActiveSupport::Concern

  included do

    def overall_rating
      reviews = self.received_reviews.includes(:ratings)
      sum_ratings = reviews.map { |r| r.overall_rating }.reduce(0, :+)
      rating = reviews.size > 0 ? sum_ratings.to_f / reviews.size : sum_ratings
      rating.round(1)
    end

    def latest_reviews
      self.received_reviews.limit(5).order(created_at: :desc)
    end

  end
end