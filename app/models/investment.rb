class Investment < ApplicationRecord

  belongs_to :investor, class_name: "User"
  belongs_to :company

end
