class Rating < ApplicationRecord

  belongs_to :category
  belongs_to :review

  delegate :name, to: :category, prefix: true

end
