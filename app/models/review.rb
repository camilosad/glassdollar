class Review < ApplicationRecord

  belongs_to :founder, class_name: "User"
  belongs_to :investor, class_name: "User"
  has_many :ratings

  delegate :name, :company, to: :founder, prefix: true
  delegate :name, to: :investor, prefix: true

  accepts_nested_attributes_for :ratings, allow_destroy: true

  def overall_rating
    sum_ratings = ratings.pluck(:rate).reduce(0, :+)
    ratings.size > 0 ? sum_ratings / ratings.size : sum_ratings
  end
end
