class User < ApplicationRecord
  include Investor

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar, :styles => { :medium => "200x200>", :thumb => "70x70#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  has_many :companies, foreign_key: "founder_id"
  has_many :investments
  has_many :given_reviews, class_name: "Review", foreign_key: "founder_id"
  has_many :received_reviews, class_name: "Review", foreign_key: "investor_id"

  enum role: [:founder, :investor]

  after_initialize :set_default_role, :if => :new_record?

  scope :founders, -> { where(role: 'founder') }
  scope :investors, -> { where(role: 'investor') }

  private

  def set_default_role
    self.role ||= :founder
  end

end
