class Company < ApplicationRecord

  belongs_to :industry
  belongs_to :founder, class_name: "User"
  has_many :investments, class_name: "User"

  validates :name, presence: true

  delegate :name, to: :industry, prefix: true
  delegate :name, to: :founder, prefix: true

end
